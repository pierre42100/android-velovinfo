package org.communiquons.velovplaces;

/**
 * Application configuration
 *
 * @author Pierre HUBERT
 */

public final class Configuration {

    /**
     * API URL - URL where stations information can be retrieved
     */
    public static final String API_URL = "https://api.jcdecaux.com/vls/v2/stations?apiKey=frifk0jbxfefqqniqez09tw4jvk37wyf823b5j1i&contract=lyon";

}

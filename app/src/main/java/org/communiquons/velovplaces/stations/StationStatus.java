package org.communiquons.velovplaces.stations;

/**
 * Stations Status
 *
 * @author Pierre HUBERT
 */

public enum StationStatus {
    OPEN,
    CLOSED,
    UNKNOWN
}

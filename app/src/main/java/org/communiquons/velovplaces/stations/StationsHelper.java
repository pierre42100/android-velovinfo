package org.communiquons.velovplaces.stations;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.communiquons.velovplaces.Configuration;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Stations helper
 *
 * @author Pierre HUBERT
 */

public class StationsHelper {

    /**
     * Debug tag
     */
    private static final String TAG = StationsHelper.class.getSimpleName();

    /**
     * Volley components
     */
    private RequestQueue mRequestQueue;

    /**
     * Object constructor
     *
     * @param context Application context
     */
    public StationsHelper(Context context){
        mRequestQueue = Volley.newRequestQueue(context);
    }

    /**
     * Get and return the list of stations
     *
     * @param callback Callback function to call in case of result
     */
    public void getList(final GetStationsCallback callback){
        Log.v(TAG, "Requested to get stations list.");

        //Clear cache (we need to have the latest data)
        mRequestQueue.getCache().clear();


        EncodedStringRequest stringRequest = new EncodedStringRequest(Request.Method.GET, Configuration.API_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseResponseList(callback, response);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFetchStationsError(GetStationError.NETWORK_ERROR);
            }
        });

        mRequestQueue.add(stringRequest);
    }

    /**
     * Parse the list of stations
     *
     * @param callback Callback function to call once finished
     * @param string The list of stations, as a string
     */
    private void parseResponseList(GetStationsCallback callback, String string){

        try {
            StationsList list = new StationsList();

            JSONArray array = new JSONArray(string);

            //Parse the list of station information
            for(int i = 0; i < array.length(); i++){
                list.add(JSONToStation(array.getJSONObject(i)));
            }

            callback.onGotStationsList(list);

        } catch (JSONException e) {
            e.printStackTrace();
            callback.onFetchStationsError(GetStationError.PARSE_ERROR);
        }

    }

    /**
     * Parse a JSON object into a Station object
     *
     * @param object JSON object to parse
     * @throws JSONException If the entry could not be correctly parsed
     */
    private static Station JSONToStation(JSONObject object) throws JSONException {
        Station station = new Station();

        //Parse fields
        station.setNumber(object.getInt("number"));
        station.setContractName(object.getString("contract_name"));
        station.setName(object.getString("name"));
        station.setAddress(object.getString("address"));

        //Get position
        JSONObject positionObject = object.getJSONObject("position");
        station.setPos_latitude(positionObject.getDouble("latitude"));
        station.setPos_longitude(positionObject.getDouble("longitude"));

        station.setBanking(object.getBoolean("banking"));
        station.setBonus(object.getBoolean("bonus"));
        station.setBikeStands(object.getInt("bike_stands"));
        station.setAvailableBikeStands(object.getInt("available_bike_stands"));
        station.setAvailableBikes(object.getInt("available_bikes"));
        station.setStatus(StringToStationStatus(object.getString("status")));
        station.setLastUpdate(object.getString("last_update"));
        station.setConnected(object.getBoolean("connected"));
        station.setOverflow(object.getBoolean("overflow"));


        return station;
    }

    /**
     * Get the StationStatus matching a string
     *
     * @param string The string to parse
     * @return Matching value in enum
     */
    private static StationStatus StringToStationStatus(String string){
        switch (string){

            case "OPEN":
                return StationStatus.OPEN;

            case "CLOSED":
                return StationStatus.CLOSED;

            default:
                return StationStatus.UNKNOWN;

        }
    }

    /**
     * Encoded string requested (copied from StringRequest
     */
    private class EncodedStringRequest extends StringRequest {

        EncodedStringRequest(int method, String url, Response.Listener<String> listener,
                             @Nullable Response.ErrorListener errorListener) {
            super(method, url, listener, errorListener);
        }


        @Override
        @SuppressWarnings("DefaultCharset")
        protected Response<String> parseNetworkResponse(NetworkResponse response) {
            String parsed;
            try {
                parsed = new String(response.data, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                // Since minSdkVersion = 8, we can't call
                // new String(response.data, Charset.defaultCharset())
                // So suppress the warning instead.
                parsed = new String(response.data);
            }
            return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
        }
    }
}

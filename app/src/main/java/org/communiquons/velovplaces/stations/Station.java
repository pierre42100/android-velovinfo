package org.communiquons.velovplaces.stations;

/**
 * Single bikes station information
 *
 * @author Pierre HUBERT
 */
public class Station {

    //Private fields
    private int number;
    private String contractName;
    private String name;
    private String address;
    private double pos_latitude;
    private double pos_longitude;
    private boolean banking;
    private boolean bonus;
    private int bikeStands; //Number of bikes places
    private int availableBikeStands; //How many bikes can we park
    private int availableBikes; //How many bikes can we take
    private StationStatus status;
    private String lastUpdate;
    private boolean connected;
    private boolean overflow;


    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getPos_latitude() {
        return pos_latitude;
    }

    public void setPos_latitude(double pos_latitude) {
        this.pos_latitude = pos_latitude;
    }

    public double getPos_longitude() {
        return pos_longitude;
    }

    public void setPos_longitude(double pos_longitude) {
        this.pos_longitude = pos_longitude;
    }

    public boolean isBanking() {
        return banking;
    }

    public void setBanking(boolean banking) {
        this.banking = banking;
    }

    public boolean isBonus() {
        return bonus;
    }

    public void setBonus(boolean bonus) {
        this.bonus = bonus;
    }

    public int getBikeStands() {
        return bikeStands;
    }

    public void setBikeStands(int bikeStands) {
        this.bikeStands = bikeStands;
    }

    public int getAvailableBikeStands() {
        return availableBikeStands;
    }

    public void setAvailableBikeStands(int availableBikeStands) {
        this.availableBikeStands = availableBikeStands;
    }

    public int getAvailableBikes() {
        return availableBikes;
    }

    public void setAvailableBikes(int availableBikes) {
        this.availableBikes = availableBikes;
    }

    public StationStatus getStatus() {
        return status;
    }

    public void setStatus(StationStatus status) {
        this.status = status;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public boolean isOverflow() {
        return overflow;
    }

    public void setOverflow(boolean overflow) {
        this.overflow = overflow;
    }


}

package org.communiquons.velovplaces.stations;

import java.util.ArrayList;

/**
 * Stations list object - contains the list of stations
 *
 * @author Pierre HUBERT
 */
public class StationsList extends ArrayList<Station> {

    /**
     * Extract and return a list of stations from the list
     *
     * @param numbers The list of stations to extract
     * @return Information about selected stations
     */
    public StationsList extractNumbers(ArrayList<Integer> numbers){
        StationsList list = new StationsList();
        for(Station station : this)
            if(numbers.contains(station.getNumber()))
                list.add(station);
        return list;
    }

    /**
     * Perform a search in the list of stations and extract the list of results
     *
     * @param query The search query
     * @return The list of results
     */
    public StationsList search(String query){
        StationsList result = new StationsList();

        for(Station station : this){

            if(station.getName().toLowerCase().contains(query.toLowerCase()))
                result.add(station);

            else if(station.getAddress().toLowerCase().contains(query.toLowerCase()))
                result.add(station);

        }

        return result;
    }
}

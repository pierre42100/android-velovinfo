package org.communiquons.velovplaces.stations;

import java.util.ArrayList;

/**
 * Get stations callback
 *
 * Triggered on request result,
 * whether the result is a success or failure
 *
 * @author Pierre HUBERT
 */

public interface GetStationsCallback {

    /**
     * Method called once we have got the list of stations
     *
     * @param list The list of stations
     */
    void onGotStationsList(StationsList list);

    /**
     * This method is called if an error occurs while retrieving
     * stations list
     */
    void onFetchStationsError(GetStationError error);
}

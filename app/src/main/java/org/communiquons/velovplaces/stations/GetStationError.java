package org.communiquons.velovplaces.stations;

/**
 * Get station list errors
 *
 * @author Pierre HUBERT
 */
public enum GetStationError {

    /**
     * If a network error occurred (eg. The HTTP request could not
     * be fully filled)
     */
    NETWORK_ERROR,

    /**
     * If a parse error occurred (eg. Could not parse response from
     * server)
     */
    PARSE_ERROR

}

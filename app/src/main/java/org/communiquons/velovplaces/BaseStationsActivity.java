package org.communiquons.velovplaces;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.communiquons.velovplaces.stations.GetStationError;
import org.communiquons.velovplaces.stations.GetStationsCallback;

public abstract class BaseStationsActivity extends AppCompatActivity
        implements OnSelectStationListener, GetStationsCallback {

    /**
     * Helpers
     */
    protected SelectedStationsListHelper mSelectedStationsListHelper;


    /**
     * Adapters
     */
    protected StationRecyclerViewAdapter mAdapter;



    /**
     * Views
     */
    private ProgressBar mProgress;
    protected RecyclerView mRecyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Get helpers
        mSelectedStationsListHelper = new SelectedStationsListHelper(this);

        //Get views
        mProgress = findViewById(R.id.progressBar);
        mRecyclerView = findViewById(R.id.stationsListRecyclerView);
    }

    @Override
    @CallSuper
    public void onSelectStation(int number, boolean selected) {
        if(selected)
            mSelectedStationsListHelper.addStation(number);
        else
            mSelectedStationsListHelper.removeStation(number);

        mAdapter.refreshSelectedStationsList();
    }

    /**
     * Toggle progress bar visibility
     *
     * @param visible TRUE to make it visible / FALSE else
     */
    protected void setLoadBarVisibility(boolean visible){
        mProgress.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onFetchStationsError(GetStationError error) {
        setLoadBarVisibility(false);

        switch (error){
            case NETWORK_ERROR:
                Toast.makeText(this, R.string.err_fetch_stations_list, Toast.LENGTH_SHORT).show();
                break;

            case PARSE_ERROR:
                Toast.makeText(this, R.string.err_parse_stations_list, Toast.LENGTH_SHORT).show();
                break;

            default:
                Toast.makeText(this, R.string.err_get_stations_list, Toast.LENGTH_SHORT).show();
        }
    }
}

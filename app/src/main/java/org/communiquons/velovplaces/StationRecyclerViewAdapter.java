package org.communiquons.velovplaces;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import org.communiquons.velovplaces.stations.Station;
import org.communiquons.velovplaces.stations.StationsList;

import java.util.ArrayList;

/**
 * Stations recycler view
 *
 * Holds the management of the displaying of the view
 *
 * @author Pierre HUBERT
 */
public class StationRecyclerViewAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private StationsList mList;
    private ArrayList<Integer> mSelectedStations = new ArrayList<>();
    private SelectedStationsListHelper mSelectedStationsListHelper;
    private boolean mShowAllStations = false;
    private OnSelectStationListener onSelectStationListener;

    /**
     * Create a new adapter
     *
     * @param context Activity context (required to inflate views
     * @param list The list of stations
     */
    StationRecyclerViewAdapter(Context context, StationsList list){
        super();

        this.mContext = context;
        this.mList = list;
        this.mSelectedStationsListHelper = new SelectedStationsListHelper(mContext);

        refreshSelectedStationsList();
    }

    /**
     * Specify whether all stations should be shown or not
     *
     * @param showAll TRUE to show all stations / FALSE else
     */
    public void setShowAllStations(boolean showAll){
        this.mShowAllStations = showAll;
        refreshSelectedStationsList();
        notifyDataSetChanged();
    }

    /**
     * Check out whether adapter is showing the entire list of stations or only selected ones
     *
     * @return TRUE if all the stations are shown / FALSE else
     */
    public boolean isShowAllStations() {
        return mShowAllStations;
    }

    /**
     * Refresh the list of selected stations
     */
    public void refreshSelectedStationsList(){
        this.mSelectedStations = mSelectedStationsListHelper.getList();
    }

    /**
     * Set station select listener
     *
     * @param onSelectStationListener Select station listener
     */
    public void setOnSelectStationListener(OnSelectStationListener onSelectStationListener) {
        this.onSelectStationListener = onSelectStationListener;
    }

    @Override
    public int getItemCount() {
        if(mShowAllStations)
            return mList.size();
        else
            return this.mSelectedStations.size();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_station, viewGroup, false);
        return new StationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        //Get information about the station
        Station station;
        if(this.mShowAllStations)
            station = mList.get(i);
        else
            station = mList.extractNumbers(mSelectedStations).get(i);

        ((StationViewHolder)viewHolder).bind(
                station,
                this.mShowAllStations,
                mSelectedStations.contains(station.getNumber()));
    }

    /**
     * Single station view holder
     */
    private class StationViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {

        /**
         * General information
         */
        private int mNumber;


        /**
         * Views
         */
        private TextView mStationName;
        private TextView mStationAddress;
        private TextView mNumberBikes;
        private TextView mNumberStands;
        private CheckBox mSelectStation;

        StationViewHolder(@NonNull View itemView) {
            super(itemView);

            //Get the views
            mStationName = itemView.findViewById(R.id.stationName);
            mStationAddress = itemView.findViewById(R.id.stationAddress);
            mNumberBikes = itemView.findViewById(R.id.numberBikes);
            mNumberStands = itemView.findViewById(R.id.numberAvailableStands);
            mSelectStation = itemView.findViewById(R.id.selectCheckBox);

            mSelectStation.setOnCheckedChangeListener(this);
        }

        @SuppressLint("SetTextI18n")
        void bind(Station station, boolean show_select, boolean is_selected){
            mNumber = station.getNumber();

            mStationName.setText(station.getName());
            mStationAddress.setText(station.getAddress());

            mNumberBikes.setText(Integer.toString(station.getAvailableBikes()));
            mNumberStands.setText(Integer.toString(station.getAvailableBikeStands()));

            mSelectStation.setVisibility(show_select ? View.VISIBLE : View.GONE);
            mSelectStation.setChecked(is_selected);
        }


        @Override
        public void onCheckedChanged(CompoundButton v, boolean isChecked) {
            if(v.equals(mSelectStation)){
                if(onSelectStationListener != null)
                    onSelectStationListener.onSelectStation(mNumber, isChecked);
            }
        }
    }
}

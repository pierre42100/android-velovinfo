package org.communiquons.velovplaces;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import org.communiquons.velovplaces.stations.StationsHelper;
import org.communiquons.velovplaces.stations.StationsList;

/**
 * Search activity
 *
 * @author Pierre HUBERT
 */
public class SearchActivity extends BaseStationsActivity {

    /**
     * Debug tag
     */
    private static final String TAG = SearchActivity.class.getSimpleName();


    private Intent mLatestIntent;
    private StationsList mStationsList;
    private TextView mNoResultNotice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_search);
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Get views
        mNoResultNotice = findViewById(R.id.noResultNotice);
        mNoResultNotice.setVisibility(View.GONE);

        mLatestIntent = getIntent();
        handleLastIntent();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        mLatestIntent = intent;
        handleLastIntent();
    }


    @Override
    public void onGotStationsList(StationsList list) {
        setLoadBarVisibility(false);

        mStationsList = list;
        handleLastIntent();
    }


    /**
     * Handle search intent
     */
    private void handleLastIntent() {

        //Get the list of stations first if required
        if (mStationsList == null) {
            new StationsHelper(this).getList(this);
            return;
        }

        if (!Intent.ACTION_SEARCH.equals(mLatestIntent.getAction()))
            return;

        //Extract search query
        String query = mLatestIntent.getStringExtra(SearchManager.QUERY);

        //Perform search
        StationsList list = mStationsList.search(query);

        //Display results
        mAdapter = new StationRecyclerViewAdapter(this, list);
        mAdapter.setShowAllStations(true);
        mAdapter.setOnSelectStationListener(this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));


        //Update no result notice visibility
        mNoResultNotice.setVisibility(list.size() == 0 ? View.VISIBLE : View.GONE);
    }

}

package org.communiquons.velovplaces;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import org.communiquons.velovplaces.stations.StationsHelper;
import org.communiquons.velovplaces.stations.StationsList;

public class MainActivity extends BaseStationsActivity {

    /**
     * Debug tag
     */
    private static final String TAG = MainActivity.class.getSimpleName();


    /**
     * Views
     */
    private TextView mNoSelectedStationNotice;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);

        mNoSelectedStationNotice = findViewById(R.id.noSelectedStationsNotice);
        mNoSelectedStationNotice.setVisibility(View.GONE);

        refreshData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.show_all_stations){
            item.setChecked(!item.isChecked());
            if(mAdapter != null) {
                mAdapter.setShowAllStations(item.isChecked());
                refreshNotStationNoticeVisibility();
            }
        }

        if(item.getItemId() == R.id.action_search) {
            onSearchRequested();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSelectStation(int number, boolean selected) {
        super.onSelectStation(number, selected);
        refreshNotStationNoticeVisibility();
    }

    private void refreshNotStationNoticeVisibility(){
        mNoSelectedStationNotice.setVisibility(
                !mAdapter.isShowAllStations() && mSelectedStationsListHelper.getList().size() == 0
                        ? View.VISIBLE : View.GONE);
    }

    /**
     * Request data refresh
     */
    private void refreshData(){
        setLoadBarVisibility(true);
        new StationsHelper(getApplicationContext()).getList(this);
    }

    @Override
    public void onGotStationsList(StationsList list) {
        setLoadBarVisibility(false);

        mAdapter = new StationRecyclerViewAdapter(this, list);
        mAdapter.setOnSelectStationListener(this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));

        refreshNotStationNoticeVisibility();
    }




}

package org.communiquons.velovplaces;

/**
 * Interface used to notify about the selection of a station
 *
 * @author Pierre HUBERT
 */
public interface OnSelectStationListener {

    /**
     * Update the select status of a station
     *
     * @param number The number of the target station
     * @param selected TRUE : station selected / FALSE : station unselected
     */
    void onSelectStation(int number, boolean selected);

}

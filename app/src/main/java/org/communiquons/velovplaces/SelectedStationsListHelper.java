package org.communiquons.velovplaces;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

/**
 * Selected stations list helper
 *
 * This file manage the list of selected stations
 *
 * @author Pierre HUBERT
 */
public class SelectedStationsListHelper {

    /**
     * Debug tag
     */
    private static final String TAG = SelectedStationsListHelper.class.getSimpleName();

    /**
     * The file where stations selection will be stored
     */
    private static final String STATIONS_SELECTION_FILE = "selected_stations.json";


    private Context mContext;

    /**
     * Initialize object
     *
     * @param context The context of the application
     */
    SelectedStationsListHelper(Context context){
        mContext = context;
    }


    /**
     * Add a station to the list of selected stations
     *
     * @param number The number of the station to add
     */
    public void addStation(int number){
        ArrayList<Integer> list = getList();
        if(!list.contains(number)) {
            list.add(number);
            saveList(list);
        }
    }

    /**
     * Remove a station from the selected stations list
     *
     * @param number The number of the station to remove
     */
    public void removeStation(int number){
        ArrayList<Integer> list = getList();
        if(list.contains(number)) {
            list.remove(list.indexOf(number));
            saveList(list);
        }
    }

    /**
     * Get the list of selected stations
     *
     * @return The list of selected stations
     */
    public ArrayList<Integer> getList() {
        try {

            FileInputStream is = mContext.openFileInput(STATIONS_SELECTION_FILE);
            InputStreamReader inputStreamReader = new InputStreamReader(is, "UTF-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            StringBuilder builder = new StringBuilder();
            String line;
            while((line = bufferedReader.readLine()) != null)
                builder.append(line);

            JSONArray array = new JSONArray(builder.toString());

            //Extract the list of stations
            ArrayList<Integer> list = new ArrayList<>();
            for(int i = 0; i < array.length(); i++)
                list.add(array.getInt(i));

            is.close();

            return list;

        } catch (java.io.IOException | JSONException e) {
            e.printStackTrace();
            return new ArrayList<>(); //Return empty list
        }
    }

    /**
     * Save a new list of stations
     *
     * @param list The list of stations to save
     */
    private void saveList(ArrayList<Integer> list){

        try {

            //Put the list in a JSON object
            JSONArray array = new JSONArray();
            for(int number : list)
                array.put(number);


            FileOutputStream os = mContext.openFileOutput(
                    STATIONS_SELECTION_FILE,
                    Context.MODE_PRIVATE);

            BufferedWriter bufferedReader = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            bufferedReader.write(array.toString());
            bufferedReader.flush();

            os.close();

        } catch (java.io.IOException e) {
            e.printStackTrace();
            Log.e(TAG, "Could not save the new list of stations!");
        }

    }
}
